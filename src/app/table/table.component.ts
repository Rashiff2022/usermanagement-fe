import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css'],
})
export class TableComponent implements OnInit {
  public name: string = '';
  constructor(private route: ActivatedRoute, private router:Router) {}

  ngOnInit() {
    this.route.queryParams.subscribe(({ user }) => {
      console.log(user);
      this.name = user;
    });
  }

  addStudent(){
    this.router.navigateByUrl('StudentRegister');
  }

  addFaculty(){
    this.router.navigateByUrl('FacultyRegister');
  }
}
