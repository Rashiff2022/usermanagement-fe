import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { AdminLoginPageComponent } from './admin-login-page/admin-login-page.component';
import { AdminRegisterPageComponent } from './admin-register-page/admin-register-page.component';
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';




@NgModule({
  declarations: [
    AdminLoginPageComponent,
    AdminRegisterPageComponent,
    AdminDashboardComponent

  ],
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  exports: [
    AdminLoginPageComponent,
    AdminRegisterPageComponent,
    AdminDashboardComponent
  ]
})
export class AdminModule { }
