import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-admin-register-page',
  templateUrl: './admin-register-page.component.html',
  styleUrls: ['./admin-register-page.component.css']
})
export class AdminRegisterPageComponent {
  adminRegisterForm = new FormGroup({
    fName : new FormControl('', Validators.required),
    LName : new FormControl('', Validators.required),
    email : new FormControl('', Validators.required),
    adminId : new FormControl('', Validators.required),
    mobile : new FormControl('', Validators.required),
    password : new FormControl('', Validators.required),
    confirmPassword : new FormControl('', Validators.required)
  });

  onSubmit(){
    console.log(this.adminRegisterForm.value);
    return this.adminRegisterForm.setValue({
      fName: '', 
      LName: '',
      email: '',
      adminId: '',
      mobile: '',
      password: '',
      confirmPassword: ''
    })
  }
}
