import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.css']
})

export class AdminDashboardComponent {
  constructor(private router:Router, private route:ActivatedRoute){}

  getStudentList(){
    this.router.navigate(['/Table'],{ queryParams:{user:'student'}});
  }
  getFacultyList(){
    this.router.navigate(['/Table'],{ queryParams:{user:'faculty'}});
  }
}
