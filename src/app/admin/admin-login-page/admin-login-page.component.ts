import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
const newLocal = 'app-admin-login-page';
@Component({
  selector: newLocal,
  templateUrl: './admin-login-page.component.html',
  styleUrls: ['./admin-login-page.component.css']
})
export class AdminLoginPageComponent {
  adminLoginForm = new FormGroup({
    email : new FormControl('',Validators.required),
    password : new FormControl('',Validators.required)
  });

  onSubmit(){
    console.log(this.adminLoginForm.value);
    this.adminLoginForm.setValue({email:'',password:''});
  };

};

