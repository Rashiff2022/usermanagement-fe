import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminDashboardComponent } from './admin/admin-dashboard/admin-dashboard.component';
import { AdminLoginPageComponent } from './admin/admin-login-page/admin-login-page.component';
import { AdminRegisterPageComponent } from './admin/admin-register-page/admin-register-page.component';
import { FacultyRegisterPageComponent } from './faculty/faculty-register-page/faculty-register-page.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { StudentLoginPageComponent } from './student/student-login-page/student-login-page.component';
import { StudentRegisterPageComponent } from './student/student-register-page/student-register-page.component';
import { TableComponent } from './table/table.component';


const routes: Routes = [
  { path: '', redirectTo:'/AdminDashboard', pathMatch:'full'},
  { path: 'AdminDashboard', component: AdminDashboardComponent },
  { path: 'AdminLogin', component: AdminLoginPageComponent },
  { path: 'AdminRegister', component: AdminRegisterPageComponent },
  { path: 'StudentRegister', component:StudentRegisterPageComponent},
  { path:'StudentLogin', component:StudentLoginPageComponent},
  { path: 'FacultyRegister', component:FacultyRegisterPageComponent},
  { path: 'Table', component: TableComponent},
  { path: '**', component: PageNotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {
  
}
