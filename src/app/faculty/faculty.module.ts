import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FacultyLoginPageComponent } from './faculty-login-page/faculty-login-page.component';
import { FacultyRegisterPageComponent } from './faculty-register-page/faculty-register-page.component';
import { FacultyDashboardComponent } from './faculty-dashboard/faculty-dashboard.component';



@NgModule({
  declarations: [
    FacultyLoginPageComponent,
    FacultyRegisterPageComponent,
    FacultyDashboardComponent
  ],
  imports: [
    CommonModule
  ]
})
export class FacultyModule { }
