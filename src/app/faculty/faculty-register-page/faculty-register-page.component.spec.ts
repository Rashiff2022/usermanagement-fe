import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FacultyRegisterPageComponent } from './faculty-register-page.component';

describe('FacultyRegisterPageComponent', () => {
  let component: FacultyRegisterPageComponent;
  let fixture: ComponentFixture<FacultyRegisterPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FacultyRegisterPageComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FacultyRegisterPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
