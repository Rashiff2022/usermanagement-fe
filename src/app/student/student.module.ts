import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StudentLoginPageComponent } from './student-login-page/student-login-page.component';
import { StudentRegisterPageComponent } from './student-register-page/student-register-page.component';
import { StudentDashbordComponent } from './student-dashbord/student-dashbord.component';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    StudentLoginPageComponent,
    StudentRegisterPageComponent,
    StudentDashbordComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  exports: [
    StudentLoginPageComponent,
    StudentRegisterPageComponent,
    StudentDashbordComponent
  ]
})
export class StudentModule { }
