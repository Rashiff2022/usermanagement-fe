import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-student-register-page',
  templateUrl: './student-register-page.component.html',
  styleUrls: ['./student-register-page.component.css']
})
export class StudentRegisterPageComponent {
  constructor( private router: Router ){}
  studentRegisterForm = new FormGroup({
    fName : new FormControl('', Validators.required),
    LName : new FormControl('', Validators.required),
    email : new FormControl('', Validators.required),
    studentId : new FormControl('', Validators.required),
    mobile : new FormControl('', Validators.required),
    password : new FormControl('', Validators.required),
    confirmPassword : new FormControl('', Validators.required)
  });

  onSubmit(){
    console.log(this.studentRegisterForm.value);
    return this.studentRegisterForm.setValue({
      fName: '', 
      LName: '',
      email: '',
      studentId: '',
      mobile: '',
      password: '',
      confirmPassword: ''
    })
  }

  loginPage(){
    this.router.navigateByUrl('StudentLogin');
  }

}
