import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-student-login-page',
  templateUrl: './student-login-page.component.html',
  styleUrls: ['./student-login-page.component.css']
})

export class StudentLoginPageComponent {
  constructor( private router:Router){}

  studentLoginForm = new FormGroup({
    email : new FormControl('',Validators.required),
    password : new FormControl('',Validators.required)
  });

  onSubmit(){
    console.log(this.studentLoginForm.value);
    this.studentLoginForm.setValue({email:'',password:''});
  };

  studentRegister(){
    this.router.navigateByUrl('StudentRegister');
  }

}
